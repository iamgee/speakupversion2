       
<!-- *******************************************************

         $$$$$$\  $$$$$$\ $$$$$$$\  $$$$$$$$\ 
        $$  __$$\ \_$$  _|$$  __$$\ $$  _____|
        $$ /  \__|  $$ |  $$ |  $$ |$$ |      
        \$$$$$$\    $$ |  $$ |  $$ |$$$$$\    
         \____$$\   $$ |  $$ |  $$ |$$  __|   
        $$\   $$ |  $$ |  $$ |  $$ |$$ |      
        \$$$$$$  |$$$$$$\ $$$$$$$  |$$$$$$$$\ 
        \______/ \______|\_______/ \________|
                                            
        $$$$$$$\   $$$$$$\  $$$$$$$\          
        $$  __$$\ $$  __$$\ $$  __$$\         
        $$ |  $$ |$$ /  $$ |$$ |  $$ |        
        $$$$$$$\ |$$$$$$$$ |$$$$$$$  |        
        $$  __$$\ $$  __$$ |$$  __$$<         
        $$ |  $$ |$$ |  $$ |$$ |  $$ |        
        $$$$$$$  |$$ |  $$ |$$ |  $$ |        
        \_______/ \__|  \__|\__|  \__|     

 **********************************************************-->

        <aside class="left-sidebar" data-sidebarbg="skin5">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">

                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="./" aria-expanded="false">
                                <i class="mdi mdi-av-timer"></i>
                                <span class="hide-menu">Dashboard</span>
                            </a>
                        </li>

                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="operators" aria-expanded="false">
                                <i class="mdi mdi-account-multiple"></i>
                                <span class="hide-menu">PUV Operators</span>
                            </a>
                        </li>

                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link dropdown-toggle" href="reports" data-toggle="dropdown" aria-expanded="false" class="dropdown-toggle">
                                <i class="mdi mdi-folder-multiple"></i>
                                <span class="hide-menu">Reports</span></a>
                                <ul class="collapse list-unstyled" id="">
                                    <li>
                                        <a class="sidebar-link waves-effect waves-dark sidebar-link" href="commends" aria-expanded="false">
                                            <i class="mdi mdi-forum"></i>
                                            <span class="hide-menu">Commendations</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="sidebar-link waves-effect waves-dark sidebar-link" href="complaints" aria-expanded="false">
                                            <i class="mdi mdi-forum"></i>
                                            <span class="hide-menu">Complaints</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="sidebar-link waves-effect waves-dark sidebar-link" href="reviews" aria-expanded="false">
                                            <i class="mdi mdi-forum"></i>
                                            <span class="hide-menu">Reviews</span>
                                        </a>
                                    </li>
                                </ul>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="vehicles" aria-expanded="false">
                                <i class="mdi mdi-car"></i>
                                <span class="hide-menu">Vehicles</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="error" aria-expanded="false">
                                <i class="mdi mdi-alert-outline"></i>
                                <span class="hide-menu">404</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>
